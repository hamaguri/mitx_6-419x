# MITx 6.419x - Data Analysis: Statistical Modeling and Computation in Applications

This course is a hands-on data analysis course that introduces the interplay between statistics and computation for the analysis of real data, part of the MicroMasters Program in Statistics and Data Science.  

## Course resources
[Overview](https://learning.edx.org/course/course-v1:MITx+6.419x+2T2022/block-v1:MITx+6.419x+2T2022+type@sequential+block@u0s1_overview/block-v1:MITx+6.419x+2T2022+type@vertical+block@u0s1_overview-tab1)\
[Study materials](https://courses.edx.org/courses/course-v1:MITx+6.419x+2T2022/resources/)


## Other resources
[Online resources on R and data science](https://gist.github.com/avecNava/9ea8b490a7d17d6871d0ff94cfcb3c83)



## License
[MIT](https://choosealicense.com/licenses/mit/)
